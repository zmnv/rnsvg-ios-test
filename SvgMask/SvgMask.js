import React, { PureComponent } from 'react';

import { View } from 'react-native';

import { Svg, Defs, Rect, Circle, LinearGradient, Stop, G, Mask, Use } from 'react-native-svg';

const STOP_COLOR = 'red'; // Gradient color

const CONTAINER_SIZE = 193;
const ELEMENT_SIZE = CONTAINER_SIZE / 2;

const OpticalGradient = props => {
    return props.definitions.map(
        ({ x, y, start, end }) => [
            <Defs key="gradient">
                <LinearGradient id={`gradient-${x}-${y}`} gradientUnits="userSpaceOnUse">
                    <Stop offset="0%" stopColor={STOP_COLOR} stopOpacity={start} />
                    <Stop offset="100%" stopColor={STOP_COLOR} stopOpacity={end} />
                </LinearGradient>
            </Defs>,
            <Rect
                key="rectangle"
                x={x}
                y={y}
                width={ELEMENT_SIZE}
                height={ELEMENT_SIZE}
                fill={`url(#gradient-${x}-${y})`}
            />
        ]
    );
};

class MaskedCircleWithDashes extends PureComponent {
    render() {
        const { rotation = 180 } = this.props;
        const innerDefs = [
            // bottom left rectangle
            { x: 0, y: ELEMENT_SIZE, start: 0.9, end: 1 },
            // top left rectangle
            { x: 0, y: 0, start: 0.9, end: 0.5 },
            // top right rectangle
            { x: ELEMENT_SIZE, y: 0, start: 0.75, end: 0.5 },
            // bottom right rectangle
            { x: ELEMENT_SIZE, y: ELEMENT_SIZE, start: 0, end: 0.5 },

        ];
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#111'}}>
            <Svg height={CONTAINER_SIZE} width={CONTAINER_SIZE} fill="transparent">
                <Defs>
                    <Mask id="mask"
                        x="0"
                        y="0"
                        width={CONTAINER_SIZE}
                        height={CONTAINER_SIZE}
                        maskUnits="userSpaceOnUse"
                        maskContentUnits="userSpaceOnUse"
                    >
                        <Circle
                            r={ELEMENT_SIZE-16}
                            cx={ELEMENT_SIZE}
                            cy={ELEMENT_SIZE}
                            stroke="white" // Mask color
                            strokeWidth={8}
                            strokeDasharray="1 4"
                            // fill="white"
                        />
                    </Mask>
                </Defs>
                <G mask="url(#mask)">
                    <G rotation={rotation} origin={`${ELEMENT_SIZE}, ${ELEMENT_SIZE}`}>
                        <OpticalGradient definitions={innerDefs} />
                    </G>
                </G>
            </Svg>
            </View>
        );
    }
}

export default MaskedCircleWithDashes;
