/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import SvgMask from './SvgMask/SvgMask';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => SvgMask);
