// import React from 'react';

import Button from './Button';

export const Components = {
    button: Button,
};

export const ButtonProvider = props => (
    <Button>{props.children}</Button>
);
