import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Text, View, ActivityIndicator, TouchableOpacity } from 'react-native';

import { buttonStyles as styles, buttonVariantsStyles, titleVariantsStyles, loadingIndicatorColor } from './buttonStyles';


/** побыстрому */
class Button extends PureComponent {
    handlePress = () => {
        requestAnimationFrame(this.props.onPress);
    }

    textView() {
        const { type } = this.props;

        return (
            <Text
                numberOfLines={1}
                style={[
                    styles.title, titleVariantsStyles[type],
                ]}
            >
                {this.props.children}
            </Text>
        );
    }

    loadingIndicatorView() {
        return <ActivityIndicator size="small" color={loadingIndicatorColor} />;
    }

    render() {
        const {
            loading,
            disabled,
            onPress,
            type,
            ...otherProps
        } = this.props;

        return (
            <TouchableOpacity
                onPress={this.handlePress}
                style={[
                    styles.root,
                    buttonVariantsStyles[type],
                ]}
                {...otherProps}
            >
                {loading
                    ? this.loadingIndicatorView()
                    : this.textView()
                }
            </TouchableOpacity>
        );
    }
}

Button.propTypes = {
    /** Текст кнопки */
    children: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    /** Отключение кнопки */
    disabled: PropTypes.bool,
    /** Управление видом загрузки */
    loading: PropTypes.bool,
    type: PropTypes.oneOf(['filled', 'text']),
};

Button.defaultProps = {
    onPress: () => {},
    disabled: false,
    loading: false,
    type: 'text',
};

export default Button;
