import { StyleSheet } from 'react-native';;

export const loadingIndicatorColor = '#fff';

export const buttonStyles = StyleSheet.create({
    root: {
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 56,
        borderRadius: 4,
    },
    title: {
        color: 'red',
    },
});

export const buttonVariantsStyles = StyleSheet.create({
    filled: {
        backgroundColor: '#1A1B1F',
    },
    text: {
        backgroundColor: 'transparent',
    },
});

export const titleVariantsStyles = StyleSheet.create({
    filled: {
        color: '#777',
    },
    text: {
        color: 'blue',
    },
});
